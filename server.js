const express = require('express');
const mongoose = require('mongoose');
const config = require('./config');

const artists = require('./app/artists');
const albums = require('./app/albums');
const tracks = require('./app/tracks');
const trackHistory = require('./app/trackHistory');
const users = require('./app/user');

const app = express();

app.use(express.json());
app.use(express.static('public'));

const port = 8050;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
    app.use('/artists', artists);
    app.use('/albums', albums);
    app.use('/tracks', tracks);
    app.use('/trackHistory', trackHistory);
    app.use('/users', users);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });

});