const express = require('express');
const Track = require('../models/Track');

const router = express.Router();

router.get('/', (req, res) => {

    if (req.query.albums) {
        Track.find({album: req.query.albums})
            .then(track => res.send(track))
            .catch(() => res.sendStatus(500))
    } else {
        Track.find()
            .then(track => res.send(track))
            .catch(() => res.sendStatus(500))
    }

});

module.exports = router;